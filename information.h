#pragma once
#include "include.h"

class Information {
	public :
		string getMessage() { return message; }
		Utilisateur* getEmetteur() { return emetteur; }
		time_t getDate() { return date; }
		int getClasse() { return classe; }
		Information(Utilisateur* _emetteur, string _message) : emetteur(_emetteur), message(_message), date(time(NULL)), classe(0) {}
		virtual Information* getParent() {}
	protected :
		int classe;
	private:
		Utilisateur* emetteur;
		string message;
		time_t date;


};

ostream& operator<<(ostream& s, Information info);
