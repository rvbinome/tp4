#pragma once
#include "include.h"

class Commentaire : public Information {
	public :
		Information* getParent() { return parent; }
		Commentaire(Utilisateur* _emetteur, string _message) : Information(_emetteur, _message){
			classe = 1;
		}
	private :
		Information* parent;


};

ostream& operator<<( ostream& s, Commentaire com);
