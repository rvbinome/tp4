#include "include.h"

class foncteur_VerifUtil {
	private :
		string surnom;
		string mail;
	public :
		foncteur_VerifUtil(string _surnom, string _mail) : surnom(_surnom), mail(_mail) {

		}

		bool operator() (Utilisateur* user) {
			return user->getMail() == mail and user->getSurnom() == surnom;
		}
};

