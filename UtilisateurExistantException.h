#include "include.h"

class UtilisateurExistantException : public exception {
	private:
		int err;
		string message;

	public :
		UtilisateurExistantException(int _err, string _message) throw();
		~UtilisateurExistantException() throw() {}
		const char* what() const throw();




};
