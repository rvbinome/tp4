#include "include.h"


Utilisateur::Utilisateur(string _surnom, string _mail) : surnom(_surnom), mail(_mail) {
}

Information* Utilisateur::ajoutFacade( Information * info) {
	facade.push_back(info);
	return info;
}

Commentaire * Utilisateur::ajoutFacade( Commentaire * com) {
	facade.push_back(com);
	return com;
}

void Utilisateur::ajoutUtilisateur(string _surnom, string _mail) {
	foncteur_CreateUtil foncteur(_surnom, _mail);
	if ( any_of(listeUtilisateurs.begin(), listeUtilisateurs.end(), foncteur) ) {
		throw UtilisateurExistantException(0, "Le mail ou le surnom existe déjà");
	} else {
		listeUtilisateurs.push_back( new Utilisateur(_surnom, _mail) );
	}
}

void Utilisateur::afficherFacade() {
	foncteur_CompInfo foncteur;
	sort(facade.begin(), facade.end(), foncteur);
	for ( int i=0; i<5; i++) {
		cout<<*(facade.rbegin()+i)<<endl;
	}
}

void Utilisateur::afficherFacadeForFriend( Utilisateur* personne) {
    foncteur_VerifUtil foncteur(personne->getSurnom(), personne->getMail());
	if ( any_of(amis.begin(), amis.end(), foncteur) ) {
		afficherFacade();
	}

}

void Utilisateur::afficherFacadeFriend( Utilisateur* personne) {
    personne->afficherFacadeForFriend( this);

}

void Utilisateur::afficherLastInfo() {
	foncteur_CompInfo foncteur;
	sort(facade.begin(), facade.end(), foncteur);
	cout<<*(facade.rbegin())<<endl;
}

void Utilisateur::afficherDerniereInfoForFriend( Utilisateur* personne) {
    foncteur_VerifUtil foncteur(personne->getSurnom(), personne->getMail());
	if ( any_of(amis.begin(), amis.end(), foncteur) ) {
		afficherLastInfo();
	}

}

void Utilisateur::afficherDerniereInfoFriend( Utilisateur* personne) {
    personne->afficherDerniereInfoForFriend( this);

}

void Utilisateur::afficherSaFacade() {
	afficherFacade();
}

void Utilisateur::afficherConversation(Information *info) {
	afficherInfoEtFils(0, info);
}

void Utilisateur::afficherInfoEtFils(int i, Information *info) {
	for(int u =0; u<i; u++) {
		cout<<"-";
	}
	cout<<info<<endl;
	vector<Information*>::iterator it;
	for ( it=facade.begin(); it!=facade.end(); ++it) {
		if ( (*it)->getClasse() == 1 ) {
			if ( (*it)->getParent() == info) {
				afficherInfoEtFils(i+1, (*it)->getParent());
			}
		}
	}
}



ostream& operator<<( ostream& s, Utilisateur* user ) {
	s << user->getSurnom() <<endl;
	return s;
}
