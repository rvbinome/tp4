#pragma once
#include "include.h"


class Utilisateur {
    private:
        string surnom;
        string mail;
        vector<Utilisateur*> amis;
        vector<Information*> facade;
        vector<Utilisateur*> listeUtilisateurs;
		Utilisateur(string _surnom, string _mail);
		void afficherFacade();
		void afficherLastInfo();
	public :
		string getSurnom() { return surnom; }
		string getMail() { return mail; }

		Information * ajoutFacade( Information * info); // Pour les informations
		Commentaire * ajoutFacade( Commentaire * com);	// Pour les commentaires
		void ajoutUtilisateur(string _surnom, string _mail);

		void afficherSaFacade();
		void afficherFacadeFriend( Utilisateur* personne);
		void afficherFacadeForFriend( Utilisateur* personne);

		void afficherDerniereInfoFriend( Utilisateur* personne);
		void afficherDerniereInfoForFriend( Utilisateur* personne);

		void afficherConversation(Information *info);
		void afficherInfoEtFils(int i, Information *info);

};

ostream& operator<<( ostream& s, Utilisateur user );
