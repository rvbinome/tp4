#include "include.h"

UtilisateurExistantException::UtilisateurExistantException(int _err, string _message) throw() : err(_err), message(_message){

}

const char* UtilisateurExistantException::what() const throw() {
	return message.c_str();
}


