#include "include.h"

ostream& operator<<( ostream& s, Commentaire* com) {
	s << com->getEmetteur() << ":" <<com->getMessage()<<"/ En réponse à "<<com->getParent()->getEmetteur();
	return s;
}
